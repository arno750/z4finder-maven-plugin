package z4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Model URL finder based on requested build version.
 * 
 * @author arnaud
 *
 */
@Mojo(name = "z4-find-model", defaultPhase = LifecyclePhase.VALIDATE)
public class Z4ModelFinderMojo extends AbstractMojo {

	private static final String BIN_EXTENSION = "bin";

	final Logger logger = LoggerFactory.getLogger(Z4ModelFinderMojo.class);

	@Parameter(defaultValue = "${project}", required = true)
	MavenProject project;

	@Parameter(property = "releaseUrl", readonly = true)
	String releaseUrl;

	@Parameter(property = "snapshotUrl", readonly = true)
	String snapshotUrl;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		String modelUrl = releaseUrl;
		logger.info("Version {}", project.getVersion());
		if (project.getVersion().endsWith("-SNAPSHOT")) {
			logger.info("Handle SNAPSHOT version");

			try {
				ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(snapshotUrl).openStream());

				Path path = Files.createTempFile("maven-metadata", ".xml");
				FileOutputStream fileOutputStream = new FileOutputStream(path.toString());
				fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
				fileOutputStream.close();
				logger.info("Write {}", path.toString());

				// Get Document Builder
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();

				// Build Document
				Document document = builder.parse(Files.newInputStream(path));
				document.getDocumentElement().normalize();

				// Here comes the root node
				Element metadata = document.getDocumentElement();
				Element versioning = (Element) metadata.getElementsByTagName("versioning").item(0);
				String lastUpdated = versioning.getElementsByTagName("lastUpdated").item(0).getTextContent();
				logger.info("lastUpdated {}", lastUpdated);

				Element snapshotVersions = (Element) versioning.getElementsByTagName("snapshotVersions").item(0);
				NodeList list = snapshotVersions.getElementsByTagName("snapshotVersion");
				int length = list.getLength();
				logger.info("length {}", length);
				for (int index = 0; index < length; index++) {
					Element snapshotVersion = (Element) list.item(index);
					String updated = snapshotVersion.getElementsByTagName("updated").item(0).getTextContent();
					logger.info("updated {}", updated);
					if (updated.equals(lastUpdated)) {
						String extension = snapshotVersion.getElementsByTagName("extension").item(0).getTextContent();
						logger.info("extension {}", extension);
						if (BIN_EXTENSION.equals(extension)) {
							String value = snapshotVersion.getElementsByTagName("value").item(0).getTextContent();
							logger.info("value {}", value);
						}
					}
				}

			} catch (MalformedURLException exception) {
				throw new MojoExecutionException("Error while using the snapshot URL", exception);
			} catch (IOException exception) {
				throw new MojoExecutionException("Error while downloading from the snapshot URL", exception);
			} catch (ParserConfigurationException exception) {
				throw new MojoExecutionException("Error while initializing XML parser", exception);
			} catch (SAXException exception) {
				throw new MojoExecutionException("Error while parsing XML", exception);
			}
		}

		String key = "z4-model-url";
		project.getProperties().put(key, modelUrl);
		logger.info("Set property [{}] {}", key, modelUrl);
	}
}
