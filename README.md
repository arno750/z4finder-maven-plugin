# z4builder-maven-plugin

## Description

Ce plugin Maven détermine l'URL du modèle Z4 selon que l'on souhaite constuire une version release ou `SNAPSHOT`.

## Utilisation

Au niveau de la bibliothèque, une section est ajoutée dans le `pom.xml` :

```xml
<project ...>
...
    <properties>
...
        <z4finder.maven.version>1.0.0</z4finder.maven.version>
    </properties>
...
    </build>
        </plugins>
...
            <plugin>
                <groupId>z4</groupId>
                <artifactId>z4finder-maven-plugin</artifactId>
                <version>${z4finder.maven.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>z4-find-model</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <releaseUrl>https://artifactory/release/z4/z4model/${project.version}/z4model-${project.version}-bin.zip</releaseUrl>
                    <snapshotUrl>https://artifactory/snapshot/z4/z4model/${project.version}/</snapshotUrl>
                </configuration>
            </plugin>

        </plugins>
    </build>
</project>
```

## Fonctionnement

Ce plugin Maven contient un Mojo :

| Mojo | Phase de travail |
| :-: | :-: |
| `z4-find-model` | `validate` |


La détermination de l'URL du modèle dépend du type de version.

L'URL est placée dans la variable d'environnement `z4-model-url`, visible par les plugins Maven et utilisable dans le `pom.xml`, notamment par le plugin `download-maven-plugin`.

### version release

L'URL du modèle est directement définie par le paramètre de configuration `releaseUrl`.

### version SNAPSHOT

La détermination de l'URL du modèle nécessite de télécharger le fichier `maven-metadata.xml` qui liste les différents build d'une même version `SNAPSHOT`. Ce fichier se trouve à la racine du répertoire pointé par l'URL définie par le paramètre de configuration `snapshotUrl` :

```xml
<?xml version="1.0" encoding="UTF-8"?>
<metadata>
  <groupId>arno750</groupId>
  <artifactId>z4model</artifactId>
  <version>1.2.3-SNAPSHOT</version>
  <versioning>
    <snapshot>
      <timestamp>20220101.112233</timestamp>
      <buildNumber>3</buildNumber>
    </snapshot>
    <lastUpdated>20220101010101</lastUpdated>
  </versioning>
</metadata>
```

Il est alors possible de construire l'URL du modèle en accolant les différentes informations :

```bash
${snapshotUrl}z4model-${metadata.version}-${metadata.versioning.snapshot.timestamp}-${metadata.versioning.snapshot.buildNumber}-bin.zip
```
